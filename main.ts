let canvas = document.getElementById("myCanvas") as HTMLCanvasElement;
let ctx = canvas.getContext("2d") as CanvasRenderingContext2D;


// event values
let vk_right = false;
let vk_left = false;

// public values
let points = 10;
let score = 0;

// define event listener
document.addEventListener("keydown", keyDownHandler, false);
document.addEventListener("keyup", keyUpHandler, false)

// Event functions
function keyDownHandler(e: KeyboardEvent) {
	if (e.key == "Right" || e.key == "ArrowRight")
		vk_right = true;
	else if (e.key == "Left" || e.key == "ArrowLeft")
		vk_left = true;
}

function keyUpHandler(e: KeyboardEvent) {
	if (e.key == "Right" || e.key == "ArrowRight")
		vk_right = false;
	else if (e.key == "Left" || e.key == "ArrowLeft")
		vk_left = false;
}

// Paddle class
class Paddle{
	height: number;
	width: number;
	x: number;
	y: number;
	speed: number;

	constructor() {
		this.height = 10;
		this.width = 75;
		this.x = (canvas.width - this.width) / 2;
		this.y = canvas.height - this.height;
		this.speed = 10;
	}

	update = () => {
		if (vk_left) {
			if (this.x-this.speed >= 0)
				this.x -= this.speed;
		} else if (vk_right) {
			if (this.x+this.width <= canvas.width)
				this.x += this.speed;
		}
	}

	draw = () => {
		ctx.beginPath();
		ctx.rect(this.x, this.y, this.width, this.height);
		ctx.fillStyle = "#3600aa";
		ctx.fill();
		ctx.closePath();
	}
};

// Ball class
class Ball{
	speed: number;
	x: number;
	y: number;
	dx: number;
	dy: number;
	ballRadius: number;

	constructor() {
		this.speed = 2;
		this.x = canvas.width / 2;
		this.y = canvas.height / 2;
		this.dx = this.speed;
		this.dy = -this.speed;
		this.ballRadius = 10;
	}

	update = () => {
		if (this.y + this.dy < this.ballRadius)
			this.dy = -this.dy;
		if (this.x + this.dx > canvas.width - this.ballRadius || this.x + this.dx < this.ballRadius)
			this.dx = -this.dx;
		this.x += this.dx;
		this.y += this.dy;
		if (this.y > canvas.width) {
			points--;
			this.x = canvas.width / 2;
			this.y = canvas.height / 2;
			this.dy = this.speed;
			this.dx = -this.speed;
		}
	}

	draw = () => {
		// Circle with arc
		ctx.beginPath();
		ctx.arc(this.x, this.y, this.ballRadius, 0, 2*Math.PI, false);
		ctx.fillStyle = "#3600aa";
		ctx.fill();
		ctx.closePath();
	}

	colide = (pad: Paddle) => {
		if (this.x + this.dx > pad.x && this.x + this.dx < pad.x + pad.width) {
			if (this.y + this.dy >= pad.y && this.y + this.dy <= pad.y + pad.height) {
				this.dy = -this.dy;
				this.dx *= 1.25;
				this.dy *= 1.25;
				score += 1;
				if (this.x < pad.x + pad.width / 2) {
					if (this.dx > 0)
						this.dx = -this.dx;
				} else {
					if (this.dx < 0)
						this.dx = -this.dx;
				}
			}
		}
	}
};

// Function cls
let cls = (color: string) => {
	ctx.beginPath();
	ctx.rect(0, 0, canvas.width, canvas.height);
	ctx.fillStyle = color;
	ctx.fill();
	ctx.closePath();
}

// Draw loop

let ball = new Ball;
let paddle = new Paddle;
let draw = () => {
	cls("#FFFFFF");
	if (points <= 0) {
		cls(String('#FF0000'));
		ctx.font = '48px serif';
		ctx.strokeText("Dead", 10, 50);
		ctx.strokeText("SCORE:" + String(score), 10, 120);
	} else {
		ball.colide(paddle);
		paddle.update();
		ball.update();
		paddle.draw();
		ball.draw();
		ctx.font = '48px serif';
		ctx.strokeText("HP:" + String(points), 10, 50);
		ctx.strokeText("SCORE:" + String(score), 10, 120);
	}
}

setInterval(draw, 10);