var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
// event values
var vk_right = false;
var vk_left = false;
// public values
var points = 10;
var score = 0;
// define event listener
document.addEventListener("keydown", keyDownHandler, false);
document.addEventListener("keyup", keyUpHandler, false);
// Event functions
function keyDownHandler(e) {
    if (e.key == "Right" || e.key == "ArrowRight")
        vk_right = true;
    else if (e.key == "Left" || e.key == "ArrowLeft")
        vk_left = true;
}
function keyUpHandler(e) {
    if (e.key == "Right" || e.key == "ArrowRight")
        vk_right = false;
    else if (e.key == "Left" || e.key == "ArrowLeft")
        vk_left = false;
}
// Paddle class
var Paddle = /** @class */ (function () {
    function Paddle() {
        var _this = this;
        this.update = function () {
            if (vk_left) {
                if (_this.x - _this.speed >= 0)
                    _this.x -= _this.speed;
            }
            else if (vk_right) {
                if (_this.x + _this.width <= canvas.width)
                    _this.x += _this.speed;
            }
        };
        this.draw = function () {
            ctx.beginPath();
            ctx.rect(_this.x, _this.y, _this.width, _this.height);
            ctx.fillStyle = "#3600aa";
            ctx.fill();
            ctx.closePath();
        };
        this.height = 10;
        this.width = 75;
        this.x = (canvas.width - this.width) / 2;
        this.y = canvas.height - this.height;
        this.speed = 10;
    }
    return Paddle;
}());
;
// Ball class
var Ball = /** @class */ (function () {
    function Ball() {
        var _this = this;
        this.update = function () {
            if (_this.y + _this.dy < _this.ballRadius)
                _this.dy = -_this.dy;
            if (_this.x + _this.dx > canvas.width - _this.ballRadius || _this.x + _this.dx < _this.ballRadius)
                _this.dx = -_this.dx;
            _this.x += _this.dx;
            _this.y += _this.dy;
            if (_this.y > canvas.width) {
                points--;
                _this.x = canvas.width / 2;
                _this.y = canvas.height / 2;
                _this.dy = _this.speed;
                _this.dx = -_this.speed;
            }
        };
        this.draw = function () {
            // Circle with arc
            ctx.beginPath();
            ctx.arc(_this.x, _this.y, _this.ballRadius, 0, 2 * Math.PI, false);
            ctx.fillStyle = "#3600aa";
            ctx.fill();
            ctx.closePath();
        };
        this.colide = function (pad) {
            if (_this.x + _this.dx > pad.x && _this.x + _this.dx < pad.x + pad.width) {
                if (_this.y + _this.dy >= pad.y && _this.y + _this.dy <= pad.y + pad.height) {
                    _this.dy = -_this.dy;
                    _this.dx *= 1.25;
                    _this.dy *= 1.25;
                    score += 1;
                    if (_this.x < pad.x + pad.width / 2) {
                        if (_this.dx > 0)
                            _this.dx = -_this.dx;
                    }
                    else {
                        if (_this.dx < 0)
                            _this.dx = -_this.dx;
                    }
                }
            }
        };
        this.speed = 2;
        this.x = canvas.width / 2;
        this.y = canvas.height / 2;
        this.dx = this.speed;
        this.dy = -this.speed;
        this.ballRadius = 10;
    }
    return Ball;
}());
;
// Function cls
var cls = function (color) {
    ctx.beginPath();
    ctx.rect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = color;
    ctx.fill();
    ctx.closePath();
};
// Draw loop
var ball = new Ball;
var paddle = new Paddle;
var draw = function () {
    cls("#FFFFFF");
    if (points <= 0) {
        cls(String('#FF0000'));
        ctx.font = '48px serif';
        ctx.strokeText("Dead", 10, 50);
        ctx.strokeText("SCORE:" + String(score), 10, 120);
    }
    else {
        ball.colide(paddle);
        paddle.update();
        ball.update();
        paddle.draw();
        ball.draw();
        ctx.font = '48px serif';
        ctx.strokeText("HP:" + String(points), 10, 50);
        ctx.strokeText("SCORE:" + String(score), 10, 120);
    }
};
setInterval(draw, 10);
//# sourceMappingURL=main.js.map